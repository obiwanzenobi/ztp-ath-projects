﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation
{
    public class TrackStatisticsUpdater : ITrackStatisticsUpdateService
    {

        private IDistanceStatisticsService _distanceService;
        private ITimeStatisticsService _timeService;
        private ISpeedStatisticsService _speedService;
        private IElevationStatisticsService _elevationService;

        public TrackStatisticsUpdater(IDistanceStatisticsService distanceService, ITimeStatisticsService timeService,
            ISpeedStatisticsService speedService, IElevationStatisticsService elevationService)
        {
            _distanceService = distanceService;
            _timeService = timeService;
            _speedService = speedService;
            _elevationService = elevationService;
        }

        public void UpdateStatisticsWithSection(SectionStatistics section, TrackStatistics trackStatistics)
        {
            _distanceService.AddSectionToStatistics(section, trackStatistics.DistanceStats);
            _speedService.AddSectionToStatistics(section, trackStatistics.SpeedStats);
            _timeService.AddSectionToStatistics(section, trackStatistics.TimeStats);
            _elevationService.AddSectionToStatistics(section, trackStatistics.ElevationStats);
        }
    }
}
