﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Elevation
{
    public class BasicElevationService : IElevationStatisticsService
    {
        public void AddSectionToStatistics(SectionStatistics section, ElevationStatistics statistics)
        {
            statistics.TotalElevation += section.EndElevation;
            statistics.TotalElevationRecords++;

            CheckForNewMaximum(section, statistics);
            CheckForNewMinimum(section, statistics);

            CheckForClimbing(section, statistics);
            CheckForDescent(section, statistics);

        }

        private static void CheckForDescent(SectionStatistics section, ElevationStatistics statistics)
        {
            if (section.HeightDifference < 0)
            {
                statistics.TotalDescent += Math.Abs(section.HeightDifference);
            }
        }

        private static void CheckForClimbing(SectionStatistics section, ElevationStatistics statistics)
        {
            if (section.HeightDifference > 0)
            {
                statistics.TotalClimbing += section.HeightDifference;
            }
        }

        private static void CheckForNewMinimum(SectionStatistics section, ElevationStatistics statistics)
        {
            if (statistics.MinimumElevation > section.EndElevation 
                || statistics.MinimumElevation == 0)
            {
                statistics.MinimumElevation = section.EndElevation;
            }
        }

        private static void CheckForNewMaximum(SectionStatistics section, ElevationStatistics statistics)
        {
            if (statistics.MaximumElevation < section.EndElevation)
            {
                statistics.MaximumElevation = section.EndElevation;
            }
        }
    }
}
