﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation
{
    public class BasicSectionStatisticService : ISectionStatisticsService
    {
        public const double KILOMETERS_PER_HOURS_TO_METERS_PER_SECOND_RATIO = 3.6;
        private IFlatDistanceCalc _distanceCalc;

        public BasicSectionStatisticService(IFlatDistanceCalc distanceCalc)
        {
            _distanceCalc = distanceCalc;
        }

        public SectionStatistics GetSectionStatistics(GeoPoint from, GeoPoint to)
        {
            SectionStatistics sectionStatistic = new SectionStatistics();

            sectionStatistic.Distance = _distanceCalc.Calculate(from, to) * 1000;
            sectionStatistic.TimeElapsedInSeconds = (to.TimeOfRegistration - from.TimeOfRegistration).TotalSeconds;
            sectionStatistic.HeightDifference = from.Altitude - to.Altitude;
            sectionStatistic.Speed = (sectionStatistic.Distance / sectionStatistic.TimeElapsedInSeconds)
                * KILOMETERS_PER_HOURS_TO_METERS_PER_SECOND_RATIO;
            sectionStatistic.EndElevation = to.Altitude;

            return sectionStatistic;
        }
    }
}
