﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;

namespace TrackAnalyser.Implementation.Speed
{
    public class DescentSpeedHandler : BaseHandler<SpeedByElevationHandlerRequest>
    {
        public override void HandleRequest(SpeedByElevationHandlerRequest request)
        {
            if (request != null)
            {
                if (request.HeightDifference < 0)
                {
                    request.Statistics.TotalDescentSpeed += request.Speed;
                    request.Statistics.TotalDescentRecords++;
                }
                else if (_successor != null)
                {
                    _successor.HandleRequest(request);
                }
            }
        }
    }
}
