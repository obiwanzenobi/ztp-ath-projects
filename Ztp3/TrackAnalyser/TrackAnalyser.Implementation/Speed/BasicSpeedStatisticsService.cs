﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Speed
{
    public class BasicSpeedStatisticsService : ISpeedStatisticsService
    {
        private BaseHandler<SpeedByElevationHandlerRequest> _climbingHandler, _descentHandler, _flatHandler;

        public void AddSectionToStatistics(SectionStatistics section, SpeedStatistics statistics)
        {
            HandleSpeedByElevation(section, statistics);
            CheckForNewMaxSpeed(section, statistics);
            CheckForNewMinSpeed(section, statistics);
            AddTotalSpeed(section, statistics);

        }

        private void HandleSpeedByElevation(SectionStatistics section, SpeedStatistics statistics)
        {
            SpeedByElevationHandlerRequest handlerRequest = GetHandlerRequest(section, statistics);

            InitDistanceHandlers();
            SetupHandlersSuccessors();
            _climbingHandler.HandleRequest(handlerRequest);
        }

        private static void AddTotalSpeed(SectionStatistics section, SpeedStatistics statistics)
        {
            statistics.TotalRecords++;
            statistics.TotalSpeed += section.Speed;
        }

        private static void CheckForNewMinSpeed(SectionStatistics section, SpeedStatistics statistics)
        {
            if (section.Speed < statistics.MinimumSpeed)
            {
                statistics.MinimumSpeed = section.Speed;
            }
        }

        private static void CheckForNewMaxSpeed(SectionStatistics section, SpeedStatistics statistics)
        {
            if (section.Speed > statistics.MaximumSpeed)
            {
                statistics.MaximumSpeed = section.Speed;
            }
        }

        private SpeedByElevationHandlerRequest GetHandlerRequest(SectionStatistics section, SpeedStatistics statistics)
        {
            SpeedByElevationHandlerRequest handlerRequest = new SpeedByElevationHandlerRequest();

            handlerRequest.Speed = section.Distance;
            handlerRequest.HeightDifference = section.HeightDifference;
            handlerRequest.Statistics = statistics;

            return handlerRequest;
        }

        private void InitDistanceHandlers()
        {
            _climbingHandler = new ClimbingSpeedHandler();
            _descentHandler = new DescentSpeedHandler();
            _flatHandler = new FlatSpeedHandler();
        }

        private void SetupHandlersSuccessors()
        {
            _climbingHandler.SetSuccessor(_descentHandler);
            _descentHandler.SetSuccessor(_flatHandler);
        }
    }
}
