﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Speed
{
    public class SpeedByElevationHandlerRequest
    {
        public SpeedStatistics Statistics { get; set; }
        public double Speed { get; set; }
        public double HeightDifference { get; set; }
    }
}
