﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;

namespace TrackAnalyser.Implementation.Speed
{
    public class FlatSpeedHandler : BaseHandler<SpeedByElevationHandlerRequest>
    {
        public override void HandleRequest(SpeedByElevationHandlerRequest request)
        {
            if (request != null)
            {
                if (request.HeightDifference == 0)
                {
                    request.Statistics.TotalFlatSpeed += request.Speed;
                    request.Statistics.TotalFlatRecords++;
                }
                else if (_successor != null)
                {
                    _successor.HandleRequest(request);
                }
            }
        }
    }
}
