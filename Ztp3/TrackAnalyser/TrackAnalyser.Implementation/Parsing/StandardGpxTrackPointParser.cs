﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Parsing
{
    public class StandardGpxTrackPointParser : ITrackPointParser
    {
        public const string LATITUDE_ATRIBUTE_KEY = "lat";
        public const string LONGITUDE_ATRIBUTE_KEY = "lon";
        public const string ELEVATION_ELEMENT_KEY = "ele";
        public const string TIME_ELEMENT_KEY = "time";

        private IFormatProvider _numberFormatProvider;

        public StandardGpxTrackPointParser(IFormatProvider numberFormatProvider)
        {
            _numberFormatProvider = numberFormatProvider;
        }

        public GeoPoint GetGeoPoint(XElement element)
        {
            GeoPoint point = new GeoPoint();

            point.Latitude = Double.Parse(element.Attribute(LATITUDE_ATRIBUTE_KEY).Value, _numberFormatProvider);
            point.Longitude = Double.Parse(element.Attribute(LONGITUDE_ATRIBUTE_KEY).Value, _numberFormatProvider);

            XElement elementElev = element.Descendants().Where(x => x.Name.LocalName.Equals(ELEVATION_ELEMENT_KEY)).FirstOrDefault();
            point.Altitude = Double.Parse(elementElev.Value, _numberFormatProvider);

            XElement elementDate = element.Descendants().Where(x => x.Name.LocalName.Equals(TIME_ELEMENT_KEY)).FirstOrDefault();
            point.TimeOfRegistration = DateTime.Parse(elementDate.Value);

            return point;
        }
    }
}
