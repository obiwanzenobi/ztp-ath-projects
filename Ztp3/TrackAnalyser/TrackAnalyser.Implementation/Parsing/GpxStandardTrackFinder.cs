﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Parsing
{
    public class GpxStandardTrackFinder : ITrackPointsFinder
    {
        private ITrackPointParser _parser;

        public GpxStandardTrackFinder(ITrackPointParser parser)
        {
            _parser = parser;
        }

        public IEnumerable<GeoPoint> GetPoints(XDocument document)
        {
            XElement track = document.Descendants().Where(x => x.Name.LocalName.Equals("trk")).FirstOrDefault();
            XElement trackSegment = track.Descendants().Where(x => x.Name.LocalName.Equals("trkseg")).FirstOrDefault();
            IEnumerable<XElement> trkpts = trackSegment.Descendants().Where(x => x.Name.LocalName.Equals("trkpt"));
            IEnumerable<GeoPoint> childs = trkpts.Select(x =>
                {
                    return _parser.GetGeoPoint(x);
                }).AsEnumerable(); ;

            return childs;
        }
    }
}
