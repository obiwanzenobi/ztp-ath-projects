﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Time
{
    public class TimeByElevationHandlerRequest
    {
        public TimeStatistics Statistics { get; set; }
        public double HeightDifference { get; set; }
        public double Time { get; set; }
    }
}
