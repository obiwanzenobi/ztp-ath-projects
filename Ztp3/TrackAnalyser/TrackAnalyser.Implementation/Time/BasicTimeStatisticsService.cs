﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Time
{
    public class BasicTimeStatisticsService : ITimeStatisticsService
    {
        private BaseHandler<TimeByElevationHandlerRequest> _climbingHandler, _descentHandler, _flatHandler;

        public void AddSectionToStatistics(SectionStatistics section, TimeStatistics statistics)
        {
            SetTimeByClimbing(section, statistics);
        }

        private void SetTimeByClimbing(SectionStatistics section, TimeStatistics statistics)
        {
            TimeByElevationHandlerRequest handlerRequest = new TimeByElevationHandlerRequest();

            handlerRequest.Statistics = statistics;
            handlerRequest.Time = section.TimeElapsedInSeconds;
            handlerRequest.HeightDifference = section.HeightDifference;

            InitHandlers();
            SetHandlersSuccessors();
            _climbingHandler.HandleRequest(handlerRequest);
        }

        private void SetHandlersSuccessors()
        {
            _climbingHandler.SetSuccessor(_descentHandler);
            _descentHandler.SetSuccessor(_flatHandler);
        }

        private void InitHandlers()
        {
            _climbingHandler = new ClimbingTimeHandler();
            _descentHandler = new DescentTimeHandler();
            _flatHandler = new FlatTimeHandler();
        }

    }
}
