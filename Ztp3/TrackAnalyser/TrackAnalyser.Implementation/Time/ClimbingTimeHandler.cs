﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;

namespace TrackAnalyser.Implementation.Time
{
    public class ClimbingTimeHandler : BaseHandler<TimeByElevationHandlerRequest>
    {
        public override void HandleRequest(TimeByElevationHandlerRequest request)
        {

            if (request != null)
            {
                if (request.HeightDifference > 0)
                {
                    request.Statistics.ClimbingTime += request.Time;
                }
                else if (_successor != null)
                {
                    _successor.HandleRequest(request);
                }
            }

        }
    }
}
