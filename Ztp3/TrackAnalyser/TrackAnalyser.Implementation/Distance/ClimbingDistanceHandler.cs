﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;

namespace TrackAnalyser.Implementation.Distance
{
    public class ClimbingDistanceHandler : BaseHandler<DistanceByElevationHandlerRequest>
    {
        public override void HandleRequest(DistanceByElevationHandlerRequest request)
        {
            if (request != null)
            {
                if (request.HeightDifference > 0)
                {
                    request.Statistics.ClimbingDistance += request.Distance;
                }
                else if (_successor != null)
                {
                    _successor.HandleRequest(request);
                }
            }
        }
    }
}
