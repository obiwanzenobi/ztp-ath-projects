﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Distance
{
    public class BasicDistanceStatisticsService : IDistanceStatisticsService
    {
        private BaseHandler<DistanceByElevationHandlerRequest> _climbingHandler, _descentHandler, _flatHandler;

        public void AddSectionToStatistics(SectionStatistics section, DistanceStatistics statistics)
        {
            DistanceByElevationHandlerRequest handlerRequest = GetHandlerRequest(section, statistics);

            InitDistanceHandlers();
            SetupHandlersSuccessors();
            _climbingHandler.HandleRequest(handlerRequest);
        }

        private static DistanceByElevationHandlerRequest GetHandlerRequest(SectionStatistics section, DistanceStatistics statistics)
        {
            DistanceByElevationHandlerRequest handlerRequest = new DistanceByElevationHandlerRequest();

            handlerRequest.Distance = section.Distance;
            handlerRequest.HeightDifference = section.HeightDifference;
            handlerRequest.Statistics = statistics;
            return handlerRequest;
        }

        private void InitDistanceHandlers()
        {
            _climbingHandler = new ClimbingDistanceHandler();
            _descentHandler = new DescentDistanceHandler();
            _flatHandler = new FlatDistanceHandler();
        }

        private void SetupHandlersSuccessors()
        {
            _climbingHandler.SetSuccessor(_descentHandler);
            _descentHandler.SetSuccessor(_flatHandler);
        }
    }
}
