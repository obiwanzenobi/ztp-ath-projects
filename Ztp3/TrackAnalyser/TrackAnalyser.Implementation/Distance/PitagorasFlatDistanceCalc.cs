﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Distance
{
    public class PitagorasFlatDistanceCalc : IFlatDistanceCalc
    {
        public double Calculate(GeoPoint from, GeoPoint to)
        {
            double distanceLat = Math.Pow(from.Latitude - to.Latitude, 2);
            double distanceLong = Math.Pow(from.Longitude - to.Longitude, 2);

            return Math.Sqrt(distanceLat + distanceLong) / 1000;
        }
    }
}
