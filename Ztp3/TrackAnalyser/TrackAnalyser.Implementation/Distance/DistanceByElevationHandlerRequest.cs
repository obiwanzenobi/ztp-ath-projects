﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Distance
{
    public class DistanceByElevationHandlerRequest
    {
        public DistanceStatistics Statistics { get; set; }
        public double HeightDifference { get; set; }
        public double Distance { get; set; }
    }
}
