﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation.Distance
{
    public class HaversineFlatDistanceCalc : IFlatDistanceCalc
    {
        private const double EARTH_RADIUS_KM = 6371;

        public double Calculate(GeoPoint from, GeoPoint to)
        {
            double distanceLat = ToRadians(to.Latitude - from.Latitude);
            double distanceLon = ToRadians(to.Longitude - from.Longitude);

            double hav = Math.Pow(Math.Sin(distanceLat / 2), 2) +
                       Math.Cos(ToRadians(from.Latitude)) * Math.Cos(ToRadians(to.Latitude)) *
                       Math.Pow(Math.Sin(distanceLon / 2), 2);

            double distanceInRadiance = 2 * Math.Atan2(Math.Sqrt(hav), Math.Sqrt(1 - hav));

            double distanceInKilometers = EARTH_RADIUS_KM * distanceInRadiance;
            return distanceInKilometers;
        }

        private double ToRadians(double input)
        {
            return input * (Math.PI / 180);
        }
    }

}
