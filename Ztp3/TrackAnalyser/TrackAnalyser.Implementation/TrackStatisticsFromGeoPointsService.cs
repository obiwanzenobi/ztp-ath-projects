﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation
{
    public class TrackStatisticsFromGeoPointsService : ITrackStatisticsService
    {
        private ISectionStatisticsService _sectionStatService;
        private TrackStatistics _trackStatistics;
        private ITrackStatisticsUpdateService _trackService;

        public TrackStatisticsFromGeoPointsService(ISectionStatisticsService sectionService, ITrackStatisticsUpdateService trackService)
        {
            
            _sectionStatService = sectionService;
            _trackService = trackService;
        }

        public TrackStatistics GetTrackStatistics(IEnumerable<GeoPoint> geoPoints)
        {
            _trackStatistics = new TrackStatistics();
            InitStats();

            int actualPointIndex = 0;
            bool hasNext = true;

            do
            {
                GeoPoint[] points = geoPoints.Skip(actualPointIndex).Take(2).ToArray();
                if(points.Length < 2)
                {
                    hasNext = false;
                    break;
                }

                GeoPoint previousPoint = points[0];
                GeoPoint actualPoint = points[1];

                SectionStatistics section = _sectionStatService.GetSectionStatistics(previousPoint, actualPoint);
                _trackService.UpdateStatisticsWithSection(section, _trackStatistics);
                actualPointIndex++;

            } while (hasNext);

            return _trackStatistics;
        }


        private void InitStats()
        {
            _trackStatistics.DistanceStats = new DistanceStatistics();
            _trackStatistics.SpeedStats = new SpeedStatistics();
            _trackStatistics.ElevationStats = new ElevationStatistics();
            _trackStatistics.TimeStats = new TimeStatistics();
        }
    }
}
