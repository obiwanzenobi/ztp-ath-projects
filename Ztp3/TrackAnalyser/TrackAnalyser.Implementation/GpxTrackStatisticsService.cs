﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Implementation
{
    public class GpxTrackStatisticsService : ITrackAnalyseService
    {
        private ITrackPointsFinder _pointFinder;
        private ITrackStatisticsService _trackService;

        public GpxTrackStatisticsService(ITrackPointsFinder finder, ITrackStatisticsService innerService)
        {
            _pointFinder = finder;
            _trackService = innerService;
        }

        public TrackStatistics AnylyseTrack(string filePath)
        {
            XDocument document = XDocument.Load(filePath);

            IEnumerable<GeoPoint> pointsEnumerable = _pointFinder.GetPoints(document);
            return _trackService.GetTrackStatistics(pointsEnumerable);
        }

    }
}
