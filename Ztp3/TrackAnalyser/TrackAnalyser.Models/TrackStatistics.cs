﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Models
{
    public class TrackStatistics
    {
        public DistanceStatistics DistanceStats { get; set; }
        public SpeedStatistics SpeedStats { get; set; }
        public ElevationStatistics ElevationStats { get; set; }
        public TimeStatistics TimeStats { get; set; }
    }
}
