﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Models
{
    public class TimeStatistics
    {
        public double ClimbingTime { get; set; }
        public double DescentTime { get; set; }
        public double FlatTime { get; set; }
        public double TotalTime
        {
            get
            {
                return FlatTime + DescentTime + ClimbingTime;
            }
        }
    }
}
