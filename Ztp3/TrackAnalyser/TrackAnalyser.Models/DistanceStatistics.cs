﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Models
{
    public class DistanceStatistics
    {
        public double FlatDistance { get; set; }
        public double ClimbingDistance { get; set; }
        public double DescentDistance { get; set; }

        public double TotalDistance
        {
            get
            {
                return FlatDistance + ClimbingDistance + DescentDistance;
            }
        }
    }
}
