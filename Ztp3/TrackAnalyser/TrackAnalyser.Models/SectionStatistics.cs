﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Models
{
    public class SectionStatistics
    {
        public double Distance { get; set; }
        public double TimeElapsedInSeconds { get; set; }
        public double HeightDifference { get; set; }
        public double EndElevation { get; set; }
        public double Speed { get; set; }
    }
}
