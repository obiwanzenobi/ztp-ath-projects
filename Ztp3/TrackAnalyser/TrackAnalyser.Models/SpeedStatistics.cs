﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Models
{
    public class SpeedStatistics
    {
        public double MinimumSpeed { get; set; }
        public double MaximumSpeed { get; set; }

        public double TotalSpeed { get; set; }
        public int TotalRecords { get; set; }
        public double AvarageSpeed
        {
            get
            {
                return TotalSpeed / TotalRecords;
            }
        }

        public double TotalClimbingSpeed { get; set; }
        public int TotalClimbingRecords { get; set; }
        public double AvarageClimbingSpeed
        {
            get
            {
                return TotalClimbingSpeed / TotalClimbingRecords;
            }
        }

        public double TotalDescentSpeed { get; set; }
        public int TotalDescentRecords { get; set; }
        public double AvarageDescentSpeed
        {
            get
            {
                return TotalDescentSpeed / TotalDescentRecords;
            }
        }

        public double TotalFlatSpeed { get; set; }
        public int TotalFlatRecords { get; set; }
        public double AvarageFlatSpeed
        {
            get
            {
                return TotalFlatSpeed / TotalFlatRecords;
            }
        }
    }
}
