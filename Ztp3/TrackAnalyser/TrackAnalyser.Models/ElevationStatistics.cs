﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Models
{
    public class ElevationStatistics
    {
        public double MinimumElevation { get; set; }
        public double MaximumElevation { get; set; }

        public double TotalElevation { get; set; }
        public int TotalElevationRecords { get; set; }
        public double AvarageElevation
        {
            get
            {
                return TotalElevation / TotalElevationRecords;
            }
        }

        public double TotalClimbing { get; set; }
        public double TotalDescent { get; set; }
        public double FinalBalance
        {
            get
            {
                return TotalClimbing - TotalDescent;
            }
        }
    }
}
