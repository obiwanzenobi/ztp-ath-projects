﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Interfaces;
using TrackAnalyser.Models;

namespace TrackAnalyser.Tools
{
    public class Analyser
    {
        private ITrackAnalyseService _analyseService;

        public Analyser()
        {
            DependencyContainer container = new DependencyContainer();
            _analyseService = container.ProvideBasicTrackAnalyseService();
        }

        public TrackStatistics Analyse(string path)
        {
            return _analyseService.AnylyseTrack(path);
        }

    }
}
