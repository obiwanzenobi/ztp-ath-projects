﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Implementation;
using TrackAnalyser.Implementation.Distance;
using TrackAnalyser.Implementation.Elevation;
using TrackAnalyser.Implementation.Parsing;
using TrackAnalyser.Implementation.Speed;
using TrackAnalyser.Implementation.Time;
using TrackAnalyser.Interfaces;

namespace TrackAnalyser.Tools
{
    public class DependencyContainer
    {
        public ITrackAnalyseService ProvideBasicTrackAnalyseService()
        {
            return new GpxTrackStatisticsService(ProvideBasicTrackPointFinder(), ProvideTrackStatisticsService());
        }

        private ITrackPointsFinder ProvideBasicTrackPointFinder()
        {
            ITrackPointsFinder finder = new GpxStandardTrackFinder(ProvideBasicGpxTrackPointsParser());
            return finder;
        }

        private ITrackStatisticsService ProvideTrackStatisticsService()
        {
            return new TrackStatisticsFromGeoPointsService(ProvideSectionStatisticsService(), ProvideStatsUpdateService());
        }

        private ISectionStatisticsService ProvideSectionStatisticsService()
        {
            return new BasicSectionStatisticService(ProvideFlatDistanceCalc());
        }

        private IFlatDistanceCalc ProvideFlatDistanceCalc()
        {
            return new HaversineFlatDistanceCalc();
        }

        private ITrackStatisticsUpdateService ProvideStatsUpdateService()
        {
            return new TrackStatisticsUpdater(
                ProvideDistanceStatsServcie(),
                ProvideTimeStatisticsService(),
                ProvideSpeedStatisticsService(),
                ProvideElevationStatisticsService());
        }

        private IDistanceStatisticsService ProvideDistanceStatsServcie()
        {
            return new BasicDistanceStatisticsService();
        }

        private ITimeStatisticsService ProvideTimeStatisticsService()
        {
            return new BasicTimeStatisticsService();
        }

        private ISpeedStatisticsService ProvideSpeedStatisticsService()
        {
            return new BasicSpeedStatisticsService();
        }

        private IElevationStatisticsService ProvideElevationStatisticsService()
        {
            return new BasicElevationService();
        }

        private ITrackPointParser ProvideBasicGpxTrackPointsParser()
        {
            return new StandardGpxTrackPointParser(ProvideBasicNumberFormatProvider());
        }

        private IFormatProvider ProvideBasicNumberFormatProvider()
        {
            return CultureInfo.InvariantCulture;
        }
    }
}
