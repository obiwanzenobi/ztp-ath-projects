﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackAnalyser.Interfaces
{
    public abstract class BaseHandler<T>
    {
        protected BaseHandler<T> _successor;

        public void SetSuccessor(BaseHandler<T> successor)
        {
            _successor = successor;
        }

        public abstract void HandleRequest(T request);

    }
}
