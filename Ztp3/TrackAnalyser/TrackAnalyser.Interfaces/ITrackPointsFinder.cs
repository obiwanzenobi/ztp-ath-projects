﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TrackAnalyser.Models;

namespace TrackAnalyser.Interfaces
{
    public interface ITrackPointsFinder
    {
        IEnumerable<GeoPoint> GetPoints(XDocument document);
    }
}
