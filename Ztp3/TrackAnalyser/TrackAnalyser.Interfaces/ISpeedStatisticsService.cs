﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Models;

namespace TrackAnalyser.Interfaces
{
    public interface ISpeedStatisticsService
    {
        void AddSectionToStatistics(SectionStatistics section, SpeedStatistics statistics);
    }
}
