﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackAnalyser.Models;

namespace TrackAnalyser.Interfaces
{
    public interface IElevationStatisticsService
    {

        void AddSectionToStatistics(SectionStatistics section, ElevationStatistics statistics);

    }
}
