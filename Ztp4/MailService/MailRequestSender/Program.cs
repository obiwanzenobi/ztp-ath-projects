﻿using MailRequestSender.Implementation;
using MailRequestSender.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestSender
{
    class Program
    {

        public const string CONTACTS_PATH_CONFIG_KEY = "ContactsPath";

        static void Main(string[] args)
        {
            TextReader reader = new StreamReader(GetContactsPath());

            IContactReader contactReader = new CsvContactReader(reader);
            IMailRequestSender sender = new MailRequestSenderService();

            ISendMailToContactsService service = new SendMailToContactsFromCsvService(contactReader, sender);
            service.SendMailToContacts();

            reader.Close();
        }

        private static string GetContactsPath()
        {
            return ConfigurationManager.AppSettings.Get(CONTACTS_PATH_CONFIG_KEY);
        }
    }
}
