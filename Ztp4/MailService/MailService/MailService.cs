﻿using Implementation;
using Quartz;
using Quartz.Impl;
using Serilog;
using System;

namespace MailService
{
    class MailService
    {

        public const string SERVICE_NAME = "MailService";
        public const string DISPLAY_NAME = "Mail Sender Service";
        public const string SERVICE_DESCRIPTION = "Mail service supposed to send x mails every y minutes";
        public const string CONTACTS_PATH_CONFIG_KEY = "ContactsPath";
        public const string MESSAGE_PATH_CONFIG_KEY = "MessagePath";

        public MailService()
        {
        }
        public void Start()
        {
            SetupLogger();
            ScheduleJob();

            Log.Information("Service started at {0}", DateTime.Now);
            Console.WriteLine("Service started at {0}", DateTime.Now);

        }
        public void Stop()
        {
            Log.Information("Service started at {0}", DateTime.Now);
            Console.WriteLine("Service stoped at {0}", DateTime.Now);
        }

        private void SetupLogger()
        {
            Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Debug()
                            .WriteTo.RollingFile("Logs/log-{Date}.txt")
                            .CreateLogger();
            Log.Information("Log created");
        }

        private void ScheduleJob()
        {
            ISchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = factory.GetScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<MailDisposerJob>()
                .WithIdentity(MailDisposerJob.JOB_NAME, MailDisposerJob.JOB_GROUP)
                .UsingJobData(MailDisposerJob.JOB_DATA_CONTACTS_PATH, GetContactsFilePath())
                .UsingJobData(MailDisposerJob.JOB__DATA_MESSAGE_PATH, GetMessageFilePath())
                .Build();
            Log.Information("Job created");

            ITrigger trigger = TriggerBuilder.Create().StartNow().
                WithSimpleSchedule(x => x.WithIntervalInMinutes(1).RepeatForever()).Build();
            Log.Information("Trigger created");

            scheduler.ScheduleJob(job, trigger);
            Log.Information("Job scheduled");
        }

        private string GetContactsFilePath()
        {
            string value = System.Configuration.ConfigurationManager.AppSettings.Get(CONTACTS_PATH_CONFIG_KEY);
            return value;
        }

        private string GetMessageFilePath()
        {
            string value = System.Configuration.ConfigurationManager.AppSettings.Get(MESSAGE_PATH_CONFIG_KEY);
            return value;
        }
    }

}

