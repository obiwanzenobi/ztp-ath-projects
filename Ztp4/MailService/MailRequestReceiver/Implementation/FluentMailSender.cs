﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using FluentMailer.Interfaces;
using Serilog;
using System.IO;
using MailRequestReceiver.Interfaces;

namespace MailREquestReceiver.Implementation
{
    public class FluentMailSender : IMailSender
    {
        private readonly IFluentMailer _fluentMailer;
        private string _messagePath;

        public FluentMailSender(IFluentMailer fluentMailer, string messagePath)
        {
            _fluentMailer = fluentMailer;
            _messagePath = messagePath;
        }

        public async void SendMail(IContact contacts)
        {
            string subject = "This not an addvertisement";
            Log.Debug("Mail subject: {0}", subject);
            //string mailBody = String.Format("<html><body>Hello {0}! Test message</body></html>", DateTime.Now);
            //Log.Debug("Mail body: {0}", mailBody);

            IFluentMailerMessageBodyCreator message = _fluentMailer.CreateMessage();
            //IFluentMailerMailSender sender = message.WithViewBody(mailBodytring 
            //string path = @"~/Views/MailView.html";
            IFluentMailerMailSender sender = message.WithView(_messagePath);

            sender.WithSubject(subject);
            sender.WithReceiver(contacts.GetMailAddress());

            await sender.SendAsync();
        }
    }
}
