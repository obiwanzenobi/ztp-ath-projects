﻿using MailRequestReceiver.Interfaces;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestReceiver.Implementation
{
    public class RabbitMailRequestReceiver : IMailRequestReceiver
    {
        IBusControl _bus;

        public RabbitMailRequestReceiver()
        {
            _bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost/"), hostcfg =>
                {
                    hostcfg.Username("guest");
                    hostcfg.Password("guest");
                });

                cfg.ReceiveEndpoint("mail_queue", endcfg =>
                {
                    endcfg.UseRateLimit(100, TimeSpan.FromMinutes(1));
                    endcfg.Consumer<MailRequestConsumer>();
                });

            });
        }

        public void StartReceiving()
        {
            _bus.Start();
        }

        public void StopReceiving()
        {
            _bus.Stop();
        }
    }
}
