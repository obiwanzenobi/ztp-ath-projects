﻿using MailRequestReceiver.Implementation;
using MailRequestReceiver.Interfaces;
using Serilog;
using System;

namespace MailRequestReceiver
{
    public class MailService
    {

        public const string SERVICE_NAME = "MailService";
        public const string DISPLAY_NAME = "Mail Sender Service";
        public const string SERVICE_DESCRIPTION = "Mail service supposed to send x mails every y minutes";
        public const string CONTACTS_PATH_CONFIG_KEY = "ContactsPath";
        public const string MESSAGE_PATH_CONFIG_KEY = "MessagePath";

        private IMailRequestReceiver _consumer;

        public MailService()
        {
            _consumer = new RabbitMailRequestReceiver();

        }
        public void Start()
        {
            SetupLogger();
            _consumer.StartReceiving();

            Log.Information("Service started at {0}", DateTime.Now);
            Console.WriteLine("Service started at {0}", DateTime.Now);

        }
        public void Stop()
        {
            Log.Information("Service started at {0}", DateTime.Now);
            Console.WriteLine("Service stoped at {0}", DateTime.Now);
            _consumer.StopReceiving();
        }

        private void SetupLogger()
        {
            Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Debug()
                            .WriteTo.RollingFile("Logs/log-{Date}.txt")
                            .CreateLogger();
            Log.Information("Log created");
        }

    }

}

