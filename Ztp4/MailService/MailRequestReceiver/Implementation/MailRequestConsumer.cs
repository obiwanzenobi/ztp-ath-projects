﻿using FluentMailer.Factory;
using MailRequestReceiver.Interfaces;
using MailREquestReceiver.Implementation;
using MassTransit;
using Models;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace MailRequestReceiver.Implementation
{
    public class MailRequestConsumer : IConsumer<Contact>
    {
        public const string MESSAGE_PATH_CONFIG_KEY = "MessagePath";

        public async Task Consume(ConsumeContext<Contact> context)
        {
            IMailSender sender = new FluentMailSender(FluentMailerFactory.Create(), GetMessageFilePath());
            sender.SendMail(context.Message);
        }

        private string GetMessageFilePath()
        {
            return ConfigurationManager.AppSettings.Get(MESSAGE_PATH_CONFIG_KEY);
        }
    }
}