﻿using MailRequestReceiver.Implementation;
using MassTransit;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace MailRequestReceiver
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<MailService>(s =>
                {
                    s.ConstructUsing(name => new MailService());
                    s.WhenStarted(ms => ms.Start());
                    s.WhenStopped(ms => ms.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription(MailService.SERVICE_DESCRIPTION);
                x.SetDisplayName(MailService.DISPLAY_NAME);
                x.SetServiceName(MailService.SERVICE_NAME);
            });

        }
    }
}
