﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestSender.Interfaces
{
    public interface IMailRequestSender
    {
        void SendMailRequest(Contact contact);
    }
}
