﻿using Akka.Actor;
using Akka.Routing;
using MailRequestReceiver.Implementation;
using MailRequestSender.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestSender.Implementation
{
    public class ParserActor : ReceiveActor
    {

        public ParserActor()
        {
            Receive<PathMessage>(message => Handle(message));
        }

        public void Handle(PathMessage message)
        {
            TextReader reader = new StreamReader(message.Path);

            IContactReader contactReader = new CsvContactReader(reader);
            IMailRequestSender sender = new MailRequestSenderService();

            IEnumerable<Contact> contacts = contactReader.GetContactList();

            var props = Props.Create<MailSenderActor>().WithRouter(new RoundRobinPool(5));
            var actor = Context.ActorOf(props, "SenderActor");

            foreach (Contact contact in contacts)
            {
                actor.Tell(contact);
            }

            reader.Close();
        }
    }
}
