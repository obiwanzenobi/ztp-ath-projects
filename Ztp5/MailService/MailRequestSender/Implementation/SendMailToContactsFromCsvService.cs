﻿using MailRequestSender.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestSender.Implementation
{
    public class SendMailToContactsFromCsvService : ISendMailToContactsService
    {
        private IContactReader _contactReader;
        private IMailRequestSender _mailRequestSender;

        public SendMailToContactsFromCsvService(IContactReader contactReader, IMailRequestSender sender)
        {
            _contactReader = contactReader;
            _mailRequestSender = sender;
        }

        public void SendMailToContacts()
        {
            IEnumerable<Contact> contacts = _contactReader.GetContactList();

            foreach (Contact contact in contacts)
            {
                _mailRequestSender.SendMailRequest(contact);

            }
        }
    }
}
