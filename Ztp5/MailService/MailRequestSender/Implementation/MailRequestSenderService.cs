﻿using MailRequestSender.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using MassTransit;

namespace MailRequestSender.Implementation
{
    public class MailRequestSenderService : IMailRequestSender
    {
        public void SendMailRequest(Contact contact)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost/"), hostcfg =>
                {
                    hostcfg.Username("guest");
                    hostcfg.Password("guest");
                });
            });
            var endpoint = bus.GetSendEndpoint(new Uri("rabbitmq://localhost/second_mail_queue")).Result;

            bus.Start();

            endpoint.Send(contact);

            bus.Stop();
        }
    }
}
