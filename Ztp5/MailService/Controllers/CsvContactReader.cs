﻿using CsvHelper;
using CsvHelper.Configuration;
using Interfaces;
using Models;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementation
{
    public class CsvContactReader : IContactReader
    {
        private TextReader _reader;

        public CsvContactReader(TextReader reader)
        {
            _reader = reader;
        }

        public IEnumerable<Contact> GetContactList()
        {
            CsvConfiguration config = new CsvConfiguration();
            config.HasHeaderRecord = false;

            Log.Information("Setting csv reader");
          
            CsvReader csvReader = new CsvReader(_reader, config);
            IEnumerable<Contact> contacts = csvReader.GetRecords<Contact>();

            csvReader.ClearRecordCache();

            Log.Debug("Contacts file is readed, returning results...");
            return contacts;
        }

    }
}
