﻿using Akka.Actor;
using MailRequestSender.Implementation;
using Models;
using Quartz;
using Quartz.Impl;
using Serilog;
using System;

namespace MailService
{
    class MailService
    {

        public const string SERVICE_NAME = "MailService";
        public const string DISPLAY_NAME = "Mail Sender Service";
        public const string SERVICE_DESCRIPTION = "Mail service supposed to send x mails every y minutes";
        public const string CONTACTS_PATH_CONFIG_KEY = "ContactsPath";
        public const string MESSAGE_PATH_CONFIG_KEY = "MessagePath";

        public MailService()
        {
        }
        public void Start()
        {
            SetupLogger();
            ScheduleJob();

            Log.Information("Service started at {0}", DateTime.Now);
            Console.WriteLine("Service started at {0}", DateTime.Now);

        }
        public void Stop()
        {
            Log.Information("Service started at {0}", DateTime.Now);
            Console.WriteLine("Service stoped at {0}", DateTime.Now);
        }

        private void SetupLogger()
        {
            Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Debug()
                            .WriteTo.RollingFile("Logs/log-{Date}.txt")
                            .CreateLogger();
            Log.Information("Log created");
        }

        private void ScheduleJob()
        {
            var system = ActorSystem.Create("MailSenderActorSystem");
            var actor = system.ActorOf<ParserActor>("ParserActor");
            actor.Tell(new PathMessage(GetContactsFilePath()));
        }

        private string GetContactsFilePath()
        {
            string value = System.Configuration.ConfigurationManager.AppSettings.Get(CONTACTS_PATH_CONFIG_KEY);
            return value;
        }

        private string GetMessageFilePath()
        {
            string value = System.Configuration.ConfigurationManager.AppSettings.Get(MESSAGE_PATH_CONFIG_KEY);
            return value;
        }
    }

}

