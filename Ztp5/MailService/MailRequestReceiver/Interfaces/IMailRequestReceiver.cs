﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestReceiver.Interfaces
{
    public interface IMailRequestReceiver
    {
        void StartReceiving();
        void StopReceiving();
    }
}
