﻿using Akka.Actor;
using FluentMailer.Factory;
using MailRequestReceiver.Interfaces;
using MailREquestReceiver.Implementation;
using Models;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRequestReceiver.Implementation
{
    public class MailSenderActor : ReceiveActor
    {
        public const string MESSAGE_PATH_CONFIG_KEY = "MessagePath";
        public Guid guid = Guid.NewGuid();

        public MailSenderActor()
        {
            Receive<Contact>(contact => Handle(contact));
        }

        private void Handle(Contact contact)
        {
            IMailSender sender = new FluentMailSender(FluentMailerFactory.Create(), GetMessageFilePath());
            sender.SendMail(contact);

            Console.WriteLine("Mail sended from " + guid);
        }

        private string GetMessageFilePath()
        {
            return ConfigurationManager.AppSettings.Get(MESSAGE_PATH_CONFIG_KEY);
        }
    }
}
