﻿using Implementation;
using Quartz;
using Quartz.Impl;
using Serilog;
using Topshelf;

namespace MailService
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<MailService>(s =>
                {
                    s.ConstructUsing(name => new MailService());
                    s.WhenStarted(ms => ms.Start());
                    s.WhenStopped(ms => ms.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription(MailService.SERVICE_DESCRIPTION);
                x.SetDisplayName(MailService.DISPLAY_NAME);
                x.SetServiceName(MailService.SERVICE_NAME);
            });

        }

    }
}
