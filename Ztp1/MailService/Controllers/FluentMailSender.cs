﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using FluentMailer.Interfaces;
using Serilog;
using System.IO;

namespace Implementation
{
    public class FluentMailSender : IMailSender
    {
        private readonly IFluentMailer _fluentMailer;
        private string _messagePath;

        public FluentMailSender(IFluentMailer fluentMailer, string messagePath)
        {
            _fluentMailer = fluentMailer;
            _messagePath = messagePath;
        }

        public void SendMail(IList<Contact> contacts)
        {
            string subject = "This not an addvertisement";
            Log.Debug("Mail subject: {0}", subject);
            //string mailBody = String.Format("<html><body>Hello {0}! Test message</body></html>", DateTime.Now);
            //Log.Debug("Mail body: {0}", mailBody);

            IFluentMailerMessageBodyCreator message = _fluentMailer.CreateMessage();
            //IFluentMailerMailSender sender = message.WithViewBody(mailBodytring 
            //string path = @"~/Views/MailView.html";
            IFluentMailerMailSender sender = message.WithView(_messagePath);

            string[] array = contacts.Select(x => x.MailAddress).ToArray();
            Log.Debug("Receivers: {0}", array);

            sender.WithSubject(subject);
            foreach (string mail in array)
            {
                sender.WithReceiver(mail);

                sender.Send();
            }
        }
    }
}
