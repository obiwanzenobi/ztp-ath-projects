﻿using FluentMailer.Factory;
using Interfaces;
using Models;
using Quartz;
using Quartz.Impl;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementation
{
    public class MailDisposerJob : IInterruptableJob
    {

        public const int MAIL_PER_EXECUTE = 100;
        public const string JOB_NAME = "Mailer";
        public const string JOB_GROUP = "MailGroup";
        public const string JOB_DATA_CONTACTS_PATH = "pathToContacts";
        public const string JOB__DATA_MESSAGE_PATH = "pathToMessage";

        public void Execute(IJobExecutionContext context)
        {

            Log.Information("Starting MailDisposer");
            string contactsPath = (string)context.MergedJobDataMap[JOB_DATA_CONTACTS_PATH];
            TextReader reader = new StreamReader(contactsPath);

            Log.Debug("Opened file {0}", contactsPath);
            IList<Contact> actualContactList = GetThisIterationContacts(reader);
            Log.Information("Contacts in current job: {0}", actualContactList.Count);

            if (actualContactList.Count == 0)
            {
                //end of contact list, break the job
                Log.Debug("End of contact list. ---Breaking the job---");
                Properties.Settings.Default.LastReadedLine = 0;
                Properties.Settings.Default.Save();

                Interrupt();
                return;
            }

            string messagePath = (string)context.MergedJobDataMap[JOB__DATA_MESSAGE_PATH];

            IMailSender sender = new FluentMailSender(FluentMailerFactory.Create(), messagePath);
            sender.SendMail(actualContactList);

            Properties.Settings.Default.LastReadedLine += MAIL_PER_EXECUTE;
            Log.Debug("Saving last readed line...");
            Properties.Settings.Default.Save();

            reader.Close();
            Log.Information("File stream was closed");
        }

        private IList<Contact> GetThisIterationContacts(TextReader reader)
        {
            IContactReader contactReader = new CsvContactReader(reader);
            IEnumerable<Contact> contacts = contactReader.GetContactList();

            int lastLine = Properties.Settings.Default.LastReadedLine;
            Log.Debug("Line in the file which we are going to start from: {0}", lastLine);

            IList<Contact> actualContactList = contacts.Skip(lastLine).Take(MAIL_PER_EXECUTE).ToList();
            return actualContactList;
        }

        public void Interrupt()
        {
            JobKey jobKey = new JobKey(JOB_NAME, JOB_GROUP);

            ISchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = factory.GetScheduler();

            Log.Debug("Deleting job...");
            scheduler.DeleteJob(jobKey);
        }

    }
}
