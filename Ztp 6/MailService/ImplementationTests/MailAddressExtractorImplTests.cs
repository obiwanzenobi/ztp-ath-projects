﻿using NUnit.Framework;
using Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Interfaces;

namespace Implementation.Tests
{
    [TestFixture()]
    public class MailAddressExtractorImplTests
    {
        [Test()]
        public void GetAddressArrayTest()
        {
            IList<Contact> contacts = new List<Contact>();
            contacts.Add(GetTestContact());

            IMailAddressExtractor extractor = new MailAddressExtractorImpl();
            string[] extractedArray = extractor.GetAddressArray(contacts);

            foreach (var contact in contacts)
            {
                if (!extractedArray.Contains(contact.MailAddress))
                {
                    Assert.Fail();
                }

            }
        }

        private static Contact GetTestContact()
        {
            Contact contact = new Contact();

            contact.FirstName = "Test";
            contact.LastName = "Testowy";
            contact.MailAddress = "test@testowy.pl";

            return contact;
        }
    }
}