﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IMailAddressExtractor
    {
        string[] GetAddressArray(ICollection<Contact> contacts);
    }
}
