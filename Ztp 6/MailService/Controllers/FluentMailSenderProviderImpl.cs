﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMailer.Interfaces;

namespace Implementation
{
    public class FluentMailSenderProviderImpl : IFluentMailerMailSenderProvider
    {
        private IFluentMailer _mailer;

        public FluentMailSenderProviderImpl(IFluentMailer mailer)
        {
            _mailer = mailer;
        }

        public IFluentMailerMailSender ProvideSender(string textMessage)
        {
            IFluentMailerMessageBodyCreator message = _mailer.CreateMessage();
            IFluentMailerMailSender sender = message.WithViewBody(textMessage);

            return sender;
        }
    }
}
