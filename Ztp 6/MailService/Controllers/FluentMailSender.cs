﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using FluentMailer.Interfaces;
using Serilog;
using System.IO;

namespace Implementation
{
    public class FluentMailSender : IMailSender
    {
        private readonly IFluentMailerMailSender _fluentMailerSender;

        public FluentMailSender(IFluentMailerMailSender fluentMailer)
        {
            _fluentMailerSender = fluentMailer;
        }

        public void SendMail(IList<string> contacts)
        {
            //string subject = "This not an addvertisement";                       
            //Log.Debug("Mail subject: {0}", subject);
            //string mailBody = String.Format("<html><body>Hello {0}! Test message</body></html>", DateTime.Now);
            //Log.Debug("Mail body: {0}", mailBody);

            //IFluentMailerMessageBodyCreator message = _fluentMailerSender.CreateMessage();
            //IFluentMailerMailSender sender = message.WithViewBody(_message);

            //string[] array = contacts.Select(x => x.MailAddress).ToArray();
            //Log.Debug("Receivers: {0}", array);

            //sender.WithSubject(subject);
            foreach (string mail in contacts)
            {
                _fluentMailerSender.WithReceiver(mail);

                _fluentMailerSender.Send();
            }
        }
    }
}
