﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Implementation
{
    public class MailAddressExtractorImpl : IMailAddressExtractor
    {
        public string[] GetAddressArray(ICollection<Contact> contacts)
        {
            string[] array = contacts.Select(x => x.MailAddress).ToArray();
            return array;
        }
    }
}
