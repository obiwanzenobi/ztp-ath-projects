﻿using PizzaService.Core.Abstracts;
using PizzaService.Core.Interfaces;
using PizzaService.Core.Models;
using PizzaService.Implementation;
using PizzaService.Implementation.LanguageHandlers;
using PizzaService.Implementation.Services;
using PizzaService.Infrastructure.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PizzaService.Controllers
{
    [RoutePrefix("api/PizzaList")]
    public class PizzaListController : ApiController
    {
        [Route("{languageCode}")]
        [HttpGet]
        public HttpResponseMessage Get(string languageCode)
        {

            IPizzaResponseListService pizzaListService = new TranslatedPizzaListResponseService(languageCode);

            return Request.CreateResponse(HttpStatusCode.OK, pizzaListService.GetPizzaListResponse(), "application/json");
        }

    }
}
