﻿using PizzaService.Core.Abstracts;
using PizzaService.Core.Interfaces;
using PizzaService.Core.Models;
using PizzaService.Implementation;
using PizzaService.Implementation.LanguageHandlers;
using PizzaService.Implementation.Services;
using PizzaService.Infrastructure.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PizzaService.Controllers
{
    [RoutePrefix("api/pizza")]
    public class PizzaController : ApiController
    {
        public string Get()
        {
            return "Should use /id/language_code";
        }

        [Route("{id}/{languageCode}")]
        [HttpGet]
        public HttpResponseMessage Get(string id, string languageCode)
        {
            IPizzaResponseService pizzaService = new SinglePizzaResponseService(id, languageCode);

            return Request.CreateResponse(HttpStatusCode.OK, pizzaService.GetPizzaResponse(), "application/json");
        }

    }
}
