﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Core.Interfaces
{
    public interface IRepository<T, IdType> : IDisposable
    {
        T GetItem(IdType id);
        IEnumerable<T> GetItems();
        void Insert(T item);
        void Delete(T item);
        void Update(T item);
    }
}
