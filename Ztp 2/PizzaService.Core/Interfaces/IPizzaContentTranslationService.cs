﻿using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Core.Interfaces
{
    public interface IPizzaContentTranslationService
    {
        PizzaContent GetTranslatedPizzaContent();
    }
}
