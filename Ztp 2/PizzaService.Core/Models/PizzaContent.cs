﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Core.Models
{
    public class PizzaContent
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
    }
}
