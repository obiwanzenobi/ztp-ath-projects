﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Core.Abstracts
{
    public abstract class BaseHandlerWithResult<T>
    {
        protected BaseHandlerWithResult<T> _successor;

        public void SetSuccessor(BaseHandlerWithResult<T> successor)
        {
            _successor = successor;
        }

        public abstract T HandleRequest();
    }
}
