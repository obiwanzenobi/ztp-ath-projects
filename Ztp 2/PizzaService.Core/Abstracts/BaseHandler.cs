﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Core.Abstracts
{
    public abstract class BaseHandler
    {
        protected BaseHandler _successor;

        public void SetSuccessor(BaseHandler successor)
        {
            _successor = successor;
        }

        public abstract void HandleRequest();
    }
}
