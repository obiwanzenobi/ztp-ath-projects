﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Core.Responses
{
    public class PizzaListResponse
    {
        public PizzaListResponse(ICollection<PizzaResponse> responseArray)
        {
            PizzaArray = responseArray;
        }

        public ICollection<PizzaResponse> PizzaArray { get; set; }
    }
}
