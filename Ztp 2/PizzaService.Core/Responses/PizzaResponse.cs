﻿using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaService.Core.Responses
{
    public class PizzaResponse
    {
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
    }
}