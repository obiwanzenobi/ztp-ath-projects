﻿using PizzaService.Core.Abstracts;
using PizzaService.Core.Interfaces;
using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Implementation.LanguageHandlers
{
    public class PizzaContentTranslationService : IPizzaContentTranslationService
    {
        private string _languageCode;
        private Pizza _pizza;

        private BaseHandlerWithResult<PizzaContent> _accurateHandler, _englishHandler,
            _polishHandler, _anyLanguageHandler;

        public PizzaContentTranslationService(string languageCode, Pizza pizza)
        {
            _languageCode = languageCode;
            _pizza = pizza;

            InitHandlers();
            SetupSuccessors();
        }

        private void InitHandlers()
        {
            _accurateHandler = new AccuratePizzaContentLanguageSelector(_pizza, _languageCode);
            _englishHandler = new EnglishPizzaContentSelector(_pizza);
            _polishHandler = new PolishPizzaContentSelector(_pizza);
            _anyLanguageHandler = new AnyLanguagePizzaContentSelector(_pizza);

        }

        private void SetupSuccessors()
        {

            _accurateHandler.SetSuccessor(_englishHandler);
            _englishHandler.SetSuccessor(_polishHandler);
            _polishHandler.SetSuccessor(_anyLanguageHandler);
        }


        public PizzaContent GetTranslatedPizzaContent()
        {
            return _accurateHandler.HandleRequest();
        }
    }
}
