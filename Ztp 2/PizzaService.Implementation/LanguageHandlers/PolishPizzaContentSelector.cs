﻿using PizzaService.Core.Abstracts;
using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Implementation.LanguageHandlers
{
    public class PolishPizzaContentSelector : BaseHandlerWithResult<PizzaContent>
    {
        public const string ENGLISH_LANGUAGE_CODE = "pl-";

        private Pizza _pizza;
        public PolishPizzaContentSelector(Pizza pizza)
        {
            _pizza = pizza;
        }
        public override PizzaContent HandleRequest()
        {
            PizzaContent result = (from content in _pizza.Contents
                                   where content.LanguageCode.StartsWith(ENGLISH_LANGUAGE_CODE)
                                   select content).FirstOrDefault();
            if (result == null && _successor != null)
            {
                return _successor.HandleRequest();
            }
            return result;
        }
    }
}
