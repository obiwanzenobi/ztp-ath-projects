﻿using PizzaService.Core.Abstracts;
using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Implementation.LanguageHandlers
{
    public class AnyLanguagePizzaContentSelector : BaseHandlerWithResult<PizzaContent>
    {
        private Pizza _pizza;
        public AnyLanguagePizzaContentSelector(Pizza pizza)
        {
            _pizza = pizza;
        }
        public override PizzaContent HandleRequest()
        {
            PizzaContent result = _pizza.Contents.FirstOrDefault();
            if (result == null && _successor != null)
            {
                return _successor.HandleRequest();
            }
            return result;
        }
    }
}
