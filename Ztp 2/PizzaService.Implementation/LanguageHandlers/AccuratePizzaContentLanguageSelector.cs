﻿using PizzaService.Core.Abstracts;
using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Implementation.LanguageHandlers
{
    public class AccuratePizzaContentLanguageSelector : BaseHandlerWithResult<PizzaContent>
    {
        private Pizza _pizza;
        private string _languageCode;

        public AccuratePizzaContentLanguageSelector(Pizza pizza, string languageCode)
        {
            _pizza = pizza;
            _languageCode = languageCode;
        }

        public override PizzaContent HandleRequest()
        {
            //SELECT MANY
            PizzaContent result = (from content in _pizza.Contents
                                   where content.LanguageCode.Equals(_languageCode)
                                   select content).FirstOrDefault();
            if (result == null && _successor != null)
            {
                return _successor.HandleRequest();
            }
            return result;

        }
    }
}
