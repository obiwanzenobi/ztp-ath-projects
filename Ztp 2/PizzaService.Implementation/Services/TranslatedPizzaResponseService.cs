﻿using PizzaService.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaService.Core.Responses;
using PizzaService.Infrastructure.Mongo;
using PizzaService.Core.Models;
using PizzaService.Implementation.LanguageHandlers;

namespace PizzaService.Implementation.Services
{
    public class TranslatedPizzaResponseService : IPizzaResponseService
    {
        private string _languageCode;
        private Pizza _pizza;

        public TranslatedPizzaResponseService(Pizza pizza, string languageCode)
        {
            _languageCode = languageCode;
            _pizza = pizza;
        }

        public PizzaResponse GetPizzaResponse()
        {
            IPizzaContentTranslationService translationService = new PizzaContentTranslationService(_languageCode, _pizza);
            PizzaContent pizzaContent = translationService.GetTranslatedPizzaContent();

            PizzaResponse response = WriteResponse(_pizza, pizzaContent);

            return response;
        }

        private static PizzaResponse WriteResponse(Pizza pizza, PizzaContent pizzaContent)
        {
            PizzaResponse response = new PizzaResponse();

            response.ImageUrl = pizza.ImageUrl;
            response.Price = pizza.Price;
            response.Description = pizzaContent.Description;
            response.LanguageCode = pizzaContent.LanguageCode;
            response.Name = pizzaContent.Name;
            return response;
        }
    }
}
