﻿using PizzaService.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaService.Core.Responses;
using PizzaService.Infrastructure.Mongo;

namespace PizzaService.Implementation.Services
{
    public class TranslatedPizzaListResponseService : IPizzaResponseListService
    {
        private string _languageCode;

        public TranslatedPizzaListResponseService(string languageCode)
        {
            _languageCode = languageCode;
        }

        public PizzaListResponse GetPizzaListResponse()
        {
            IRepository<MongoPizza, string> repository = new MongoPizzaRepository();

            List<PizzaResponse> responseList = repository.GetItems().Select(x =>
            {
                IPizzaResponseService pizzaService = new TranslatedPizzaResponseService(x, _languageCode);
                return pizzaService.GetPizzaResponse();
            }).ToList();

            return new PizzaListResponse(responseList);
        }
    }
}
