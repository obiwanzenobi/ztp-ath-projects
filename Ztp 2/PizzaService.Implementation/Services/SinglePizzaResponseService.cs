﻿using PizzaService.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaService.Core.Responses;
using PizzaService.Infrastructure.Mongo;

namespace PizzaService.Implementation.Services
{
    public class SinglePizzaResponseService : IPizzaResponseService
    {
        private string _pizzaId, _languageCode;

        public SinglePizzaResponseService(string pizzaId, string languageCode)
        {
            _pizzaId = pizzaId;
            _languageCode = languageCode;
        }

        public PizzaResponse GetPizzaResponse()
        {
            IRepository<MongoPizza, string> repository = new MongoPizzaRepository();

            IPizzaResponseService responseService = new TranslatedPizzaResponseService(repository.GetItem(_pizzaId), _languageCode);

            return responseService.GetPizzaResponse();
        }
    }
}
