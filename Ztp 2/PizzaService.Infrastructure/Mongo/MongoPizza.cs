﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Infrastructure.Mongo
{
    public class MongoPizza : Pizza
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("_id")]
        public string guid { get; set; }
    }
}
