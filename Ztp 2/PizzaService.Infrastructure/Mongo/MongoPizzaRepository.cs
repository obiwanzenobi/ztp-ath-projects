﻿using MongoDB.Driver;
using PizzaService.Core.Interfaces;
using PizzaService.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaService.Infrastructure.Mongo
{
    public class MongoPizzaRepository : IRepository<MongoPizza, string>
    {

        //api/pizza/{id}/pl-PL
        // api/pizzaList/en-GB
        IMongoCollection<MongoPizza> _table;

        public MongoPizzaRepository()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            IMongoClient client = new MongoClient(connectionString);
            IMongoDatabase database = client.GetDatabase("pizzaStore");

            _table = database.GetCollection<MongoPizza>("pizza");
        }

        public void Delete(MongoPizza item)
        {
            _table.DeleteOne(x => x.guid.Equals(item.guid));
        }

        public void Dispose()
        {
            _table = null;
        }

        public MongoPizza GetItem(string id)
        {
            return _table.AsQueryable().Where(x => x.guid.Equals(id)).FirstOrDefault();
        }

        public IEnumerable<MongoPizza> GetItems()
        {
            return _table.AsQueryable();
        }

        public void Insert(MongoPizza item)
        {
            _table.InsertOne(item);
        }

        public void Update(MongoPizza item)
        {
            //todo 
            //_table.UpdateOne()
                
        }
    }
}